using LinearAlgebra
using Random
using StatsBase
using JLD
using FLoops

function generate_hamiltonian(L, t, W)
    # Define the Anderson Hamiltonian matrix
    H = zeros(ComplexF64, L, L)

    for i in 1:L
        H[i, i] = rand() * W - W / 2
        H[i, mod1(i + 1, L)] = -t
        H[i, mod1(i - 1, L)] = -t
    end

    return H
end

function IPR(eigenvector, L)
    ipr = 0
    for i in 1:L
        ipr += abs2(eigenvector[i])^2
    end
    return ipr
end

# Parameters
L = 1000  # Number of sites
t = 1.0  # Hopping matrix element
W_values = [0, 0.5 * t, 8 * t]  # Disorder width

Random.seed!(Int(round(time() * 1000)))

############# Calculation Spectrum in one-particle space ####################
file1 = jldopen("./data/Anderson_Spectrum.jld", "w")
file2 = jldopen("./data/Prob_Density.jld", "w")
file1["lattice site"] = 1:L
file2["lattice site"] = 1:L

random_state = 25#rand(1:L) #### Calculate eigenstate of random selection
file2["state"] = random_state
for W in W_values
    # Generate and diagonalize Hamiltonian
    H = generate_hamiltonian(L, t, W)
    eigenvalues, eigenvectors = eigen(H)
    file1["W = $W"] = eigenvalues
    file2["W = $W"] = log.(abs2.(eigenvectors[:, random_state]))
end
close(file1)
close(file2)

################## Calculation IPR - Distribution for different disorder strenghts ##################
Nr = 500
L_values = [1000, 100, 25]
file = jldopen("./data/IPR_Distribution.jld", "w")
for L in L_values

    for W in W_values

        @floop for _ in 1:Nr
            Random.seed!(Int(round(time() * 1000)) + rand(1:1000))
            H = generate_hamiltonian(L, t, W)
            eigenvectors = eigvecs(H)

            @reduce(IPR_values = append!([], [IPR(eigenvectors[:, i], L) for i in 1:L]))

        end

        file["L,W = $L,$W"] = IPR_values

    end
end
close(file)
################### Calculation of average IPR's in dependence of disorder parameter W/t
W_values = Float64.(LinRange(0, 12, 51))
Nr_values = [1, 15, 500]
L_values = [1000, 100, 25]
file = jldopen("./data/IPR_Average.jld", "w")
file["W"] = W_values

for L in L_values

    if L == 100
        global Nr_values = [500]
    end

    if L == 25
        global Nr_values = [500]
    end

    for Nr in Nr_values
        IPR_average = []
        for W in W_values

            @floop for _ in 1:Nr
                Random.seed!(Int(round(time() * 1000)) + rand(1:1000))
                H = generate_hamiltonian(L, t, W)
                eigenvectors = eigvecs(H)

                @reduce(IPR_value = append!([], [IPR(eigenvectors[:, i], L) for i in 1:L]))

            end

            append!(IPR_average, mean(IPR_value))

        end
        file["L,Nr = $L,$(Nr)"] = IPR_average
    end
end
close(file)
################ Calculation of Loc Length ################################

function Lyapunow(E, L, disorder)
    u_0, u_1 = BigFloat(0), BigFloat(1)
    lyap = 0
    for i in 1:L
        u_2 = BigFloat((E - disorder[i]) * u_1 + u_0)
        lyap += log(abs(u_2 / u_1))
        u_0 = u_1
        u_1 = u_2
    end
    return lyap / L
end

setprecision(BigFloat, 2048)

# Parameters
L = 10^4  # Number of sites
t = 1.0  # Hopping matrix element
W_values = [exp(y) for y in LinRange(log(2 * 10^-1), log(10^3), 51)] .* t
Nr = 1000

file = jldopen("./data/LocLength.jld", "w")
file["W"] = W_values
lyap_average = []
for W in W_values

    @floop for _ in 1:Nr
        Random.seed!(Int(round(time() * 1000)) + rand(1:1000))
        disorder = [rand(BigFloat) * W - W / 2 for i in 1:L]

        @reduce(lyap_values = append!([], Lyapunow(0.001, L, disorder)))

    end

    push!(lyap_average, mean(lyap_values))

end
file["LocLength"] = 1 ./ lyap_average
close(file)
