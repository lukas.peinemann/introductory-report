using Plots
using JLD
using LaTeXStrings

t = 1
W_values = [0, 0.5 * t, 8 * t]
############# Calculation Spectrum in one-particle space ####################
spectrum = plot(xlabel="\$n\$", ylabel="\$E_n / t\$", xlabelfontsize=14, ylabelfontsize=14, legendfontsize=12, marker=:xcross, dpi=200, minorgrid=true)
eigenstates = []
file1 = jldopen("./data/Anderson_Spectrum.jld", "r")
file2 = jldopen("./data/Prob_Density.jld", "r")
for W in W_values
    sites1 = read(file1, "lattice site")
    sites2 = read(file2, "lattice site")
    W1 = read(file1, "W = $W")
    W2 = read(file2, "W = $W")
    plot!(spectrum, sites1, W1, label="\$W/t = $W\$", linewidth=1.5, alpha=0.75)
    ### Plotting log-linear presentation of probability to observe localization #####
    state = read(file2, "state")
    push!(eigenstates, plot(sites2, W2, label=false, xlabel="\$l\$", ylabel="\$\\ln(\\,|Ψ_{\\alpha}(l)|^2\\,)\$", xlabelfontsize=20, ylabelfontsize=20, xtickfontsize=12, ytickfontsize=12, dpi=200, linewdith=1.5, minorgrid=true))
end
savefig(spectrum, "./figures/Anderson_Spectrum.svg")
for (i, W) in enumerate(W_values)
    savefig(eigenstates[i], "./figures/Prob_Density_W=$W.svg")
end
close(file1)
close(file2)
################## Calculation IPR - Distribution for different disorder strenghts #################
bins = [3000, 1500, 100]
colors = [:red, :orange, :blue]
L_values = [25, 100, 1000]
max_xs = [7.5 / 100, 0.1, 1]
file = jldopen("./data/IPR_Distribution.jld", "r")
for (W, max_x, nbins) in zip(W_values, max_xs, bins)
    distribution = histogram(xlabelfontsize=28, ylabelfontsize=28, xtickfontsize=14, ytickfontsize=14, legendfontsize=20, xlabel="\$\\mathrm{IPR}\$", ylabel="\$P(\\mathrm{IPR})\$", dpi=200, minorgrid=true)
    for (L, color) in zip(L_values, colors)
        LW = read(file, "L,W = $L,$W")
        stephist!(LW, alpha=0.8, label="L=$L", normed=true, linewidth=2, bins=LinRange(0, 1.0, nbins + 1), color=color)
        xlims!(0, max_x)
    end
    savefig(distribution, "./figures/IPR_Distribution_W=$W.svg")
end
close(file)


################### Calculation of average IPR's in dependence of disorder parameter W/t

L_values = [25, 100, 1000]
colors = [:green, :red, :orange, :purple, :blue]
average = plot(ylabel="\$[\\mathrm{IPR}]\$", xlabel="\$W/t\$", ylim=(0, 1), dpi=200, xlabelfontsize=14, ylabelfontsize=14, legendfontsize=12, minorgrid=true)
file = jldopen("./data/IPR_Average.jld", "r")
Wv = read(file, "W")
count = 1
for L in L_values

    global count
    Nr_values = []

    if L == 1000
        global Nr_values = [1, 15, 500]
    end

    if L == 100
        global Nr_values = [500]
    end

    if L == 25
        global Nr_values = [500]
    end
    for Nr in Nr_values
        NR_v = read(file, "L,Nr = $L,$(Nr)")
        plot!(Wv, NR_v, label="\$(L, N_r) = ($L, $(Nr))\$", alpha=0.7, linewdith=1.5, color=colors[count])
        count += 1
    end
end
savefig(average, "./figures/IPR_Average.svg")
close(file)

##################### Loc Length ##############

file = jldopen("./data/LocLength.jld", "r")
Wv = read(file, "W")
loclength = read(file, "LocLength")
len = length(loclength)
close(file)

W_int = 0
for i in 1:len
    if loclength[i] < 1000
        global W_int = Wv[i]
        break
    end
end

plt = plot(log.(Wv), log.(loclength), xlabel="\$\\ln(W/t)\$", ylabel="\$\\ln(ξ)\$", label="\$ξ(E/t = 0.001)\$", xlabelfontsize=14, ylabelfontsize=14, legendfontsize=12, dpi=200, alpha=0.8, color="red", minorgrid=true)
scatter!(log.(Wv), log.(loclength), label=false, color="red")
plot!(log.(Wv), log.(loclength), label=false, color="red", linewdith=1.5)
hline!([log(1000)], linestyle=:dash, label="\$\\ln(L = 1000)\$", color="black", alpha=0.6)
vline!([log(W_int)], linestyle=:dot, label="\$\\ln(W/t = $(round(W_int, digits = 2)))\$", color="black", alpha=0.6)
annotate!(1.75, 3.8, text("\$ξ \\propto (W/t)^{-2}\$", 14, :black))
annotate!(6, -0.5, text("\$ξ \\propto 1/\\ln(W/t)\$", 14, :black))
savefig(plt, "./figures/LocLength.svg")
