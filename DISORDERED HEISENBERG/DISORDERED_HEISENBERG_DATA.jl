using LinearAlgebra
using Random
using StatsBase
using JLD
using FLoops


# Global variables
J = 1

function flip(s::Integer, i::Int, j::Int)

    # XOR operation to flip the bits
    s = s ⊻ (1 << (i - 1))
    s = s ⊻ (1 << (j - 1))

    return s
end

function magnetization_basis(n_up::Int, L::Int) # Returns a Basis of a given Magnetization n_up for L Spins
    basis = []
    for s in 1:2^L
        bit_sum = 0
        for i in 1:L
            bit_sum += (s >> (i - 1)) & 1
        end
        if bit_sum == n_up
            push!(basis, s)
        end
    end
    return basis
end

function find_state(s::Integer, basis::Array) # Returns State Position of State s in Basis basis
    M = length(basis)
    b_min, b_max = 1, M

    while b_min <= b_max
        b = b_min + (b_max - b_min) ÷ 2
        if s < basis[b]
            b_max = b - 1
        elseif s > basis[b]
            b_min = b + 1
        else
            return b
        end
    end
    return -1
end

function magnetization_block_hamiltonian(m::Float64, L::Int, W::Float64) # Returns Block_Hamiltonian and respective Basis for given Magnetization m = n_up - n_down

    global J

    n_up = Int(m + L / 2.0)
    sub_basis = magnetization_basis(n_up, L)
    M = length(sub_basis)
    sub_H = zeros(M, M)

    disorder = [(rand() * W - W / 2) for i in 1:L]

    for a in 1:M
        for i in 1:L
            j = mod1(i + 1, L)

            sub_H[a, a] += disorder[i] * (((sub_basis[a] >> (i - 1)) & 1) - 1 / 2) # Diagonal Terms with random Potential
            sub_H[a, a] += J * (((sub_basis[a] >> (i - 1)) & 1) - 1 / 2) * (((sub_basis[a] >> (j - 1)) & 1) - 1 / 2) # Diagonal Terms, Interaction of a spin pair

            if ((sub_basis[a] >> (i - 1)) & 1) != ((sub_basis[a] >> (j - 1)) & 1) # Off diagobnal Term of spin flips resulting in new state
                s = flip(sub_basis[a], i, j)
                b = find_state(s, sub_basis)
                sub_H[b, a] += J / 2
            end
        end
    end
    return sub_H, sub_basis
end

function hamiltonian_spectrum(L::Int, W::Float64) # Return Hamiltonian Spectrum
    spectrum = []

    for m in -L/2:L/2

        sub_H, _ = magnetization_block_hamiltonian(m, L, W)

        eigenvalues, = eigen(sub_H)
        append!(spectrum, eigenvalues)

    end

    return sort(spectrum)
end


######### Calculation Gap Ratio Distribution ###############################
function six_closest_indices(spectrum::Array) # Returns 6 closest energies to energy density .5, important that spectrum values are sorted ascending

    num_elements = length(spectrum)
    E_min, E_max = spectrum[1], spectrum[num_elements]

    # If the spectrum contains less than 6 elements, simply return the entire spectrum
    if num_elements <= 6
        return sortperm(spectrum)
    end

    distances = abs.(spectrum .- (E_max + E_min) / 2) # Distances to energy density of eps = 0.5

    sorted_indices = sortperm(distances)
    closest_indices = sorted_indices[1:6]

    return sort(closest_indices)
end

function gap_ratio(spectrum::Array) # Returns mean of the gap ratio for closest six energies to target energy density eps = 0.5

    gap_list = []

    for i in six_closest_indices(spectrum)
        push!(gap_list, min(spectrum[i] - spectrum[i-1], spectrum[i+1] - spectrum[i]) / max(spectrum[i] - spectrum[i-1], spectrum[i+1] - spectrum[i]))
    end

    return gap_list
end

L_values = [8, 10, 12, 14]
W_values = Float64.([0.5, 3, 8, 12])
Nr_values = [10000, 5000, 2500, 1000]

for (L, Nr) in zip(L_values, Nr_values)
    file = jldopen("./data/GapRatioDistribution_L=$L.jld", "w")
    for W in W_values

        @floop for _ in 1:Nr
            Random.seed!(Int(round(time() * 1000)) + rand(1:1000))
            H_0, _ = magnetization_block_hamiltonian(0.0, L, W)
            spectrum = eigen(H_0).values

            @reduce(rn_values = append!([], gap_ratio(spectrum)))

        end
        file["W = $W"] = rn_values
    end
    close(file)
end
################ Calculation disorder average of gap ratio #################

L_values = [8, 10, 12, 14]
W_values = Float64.(2 .* [0.6, 1.0, 1.8, 2.0, 2.4, 2.7, 3.0, 3.6, 4.0, 5.0, 6.0, 8.0, 10, 12.5])
Nr_values = [10000, 5000, 2500, 1000]

file = jldopen("./data/GapRatioAverage.jld", "w")
file["W"] = W_values
for (L, Nr) in zip(L_values, Nr_values)
    rn_average = []
    for W in W_values

        @floop for _ in 1:Nr
            Random.seed!(Int(round(time() * 1000)) + rand(1:1000))
            H_0, _ = magnetization_block_hamiltonian(0.0, L, W)
            spectrum = eigen(H_0).values

            @reduce(rn_array = append!([], mean(gap_ratio(spectrum))))

        end

        append!(rn_average, mean(rn_array))

    end
    file["L = $L"] = rn_average
end
close(file)

##################### Measurement of <S^z_j>
function expected_Spin_z(state::Array, j::Int, basis::Array) # returns spin expecation value for z-component on lattice site j

    if length(state) != length(basis)
        exit()
    end

    M, S_z = length(state), 0
    for a in 1:M
        S_z += abs2(state[a]) * (((basis[a] >> (j - 1)) & 1) - 1 / 2)
    end

    return S_z
end

L_values = [8, 10, 12, 14]
W_values = Float64.([0.5, 3, 6, 8, 12])
Nr_values = [10000, 5000, 2500, 1000]


for (L, Nr) in zip(L_values, Nr_values)
    file = jldopen("./data/Sz_Expectation_L=$L.jld", "w")
    for W in W_values

        @floop for _ in 1:Nr
            Random.seed!(Int(round(time() * 1000)) + rand(1:1000))
            H_0, basis = magnetization_block_hamiltonian(0.0, L, W)
            spectrum, eigenvecs = eigen(H_0)
            relevant_states_ind = six_closest_indices(spectrum)

            @reduce(Sz_values = append!([], [(expected_Spin_z(eigenvecs[:, i], L ÷ 2, basis)) for i in relevant_states_ind]))

        end
        file["W = $W"] = Sz_values
    end
    close(file)
end


####### Testing Statistics for Gap Ratio values #################

function variance(array::Array, ovr_mean::Float64)

    mean, len = 0, length(array)
    for a in array
        mean += a
    end
    mean *= 1 / len

    var = 0
    for a in array
        var += (a - mean)^2
    end

    return var / len

end


L_values = [8, 10]
W_values = Float64.([2, 15])
Nr_maxes = [15000, 1500]

for (L, Nr_max) in zip(L_values, Nr_maxes)
    file = jldopen("./data/Statistics_L=$L.jld", "w")
    Nr_values = [exp(y) for y in LinRange(log(10), log(Nr_max), 51)]
    file["Nr_values"] = Nr_values
    for i in 1:5

        for W in W_values
            values = []
            for Nr in Nr_values
                @floop for _ in 1:Nr
                    Random.seed!(Int(round(time() * 1000)) + rand(1:1000))
                    H_0, _ = magnetization_block_hamiltonian(0.0, L, W)
                    spectrum = eigen(H_0).values

                    @reduce(rn_array = append!([], mean(gap_ratio(spectrum))))

                end
                push!(values, rn_array)
            end

            ovr_mean = mean(mean.(values))

            file["$(i)RelDeviations_$W"] = [abs(mean.(values)[i] - mean.(values)[i-1]) / ovr_mean for i in 2:51]

            file["$(i)Variances_$W"] = variance.(values, ovr_mean)

        end
    end
    close(file)
end
