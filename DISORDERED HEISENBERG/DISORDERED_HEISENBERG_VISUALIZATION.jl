using Plots
using JLD
using LaTeXStrings

function GOE(r::Float64)

    normalization = 4 / 27 # GOE case

    res = 1 / normalization * (r + r^2) / (1 + r + r^2)^(5 / 2)

    return res
end

function poisson(r::Float64)
    return 2 / (1 + r)^2
end

######### Calculation Gap Ratio Distribution ###############################
L_values = [8, 10, 12, 14]
W_values = Float64.([0.5, 3, 8, 12])
bins = [65, 60, 50, 30]
colors = [:green, :red, :blue, :orange]

for (L, nbins) in zip(L_values, bins)
    distribution = histogram(xlabel="\$r_n\$", ylabel="\$P(r_n)\$", xlabelfontsize=24, ylabelfontsize=24, xtickfontsize=12, ytickfontsize=12, legendfontsize=16, dpi=200, xlim=(0, 1), ylim=(0, Inf), minorgrid=true)
    file = jldopen("./data/GapRatioDistribution_L=$L.jld", "r")
    for (W, color) in zip(W_values, colors)
        Wv = read(file, "W = $W")
        stephist!(Wv, label="\$W/J = $W\$", alpha=0.8, normed=true, linewidth=1.5, bins=LinRange(0, 1, nbins + 1), color=color)
    end
    close(file)
    plot!(distribution, LinRange(0, 1, 101), GOE.(LinRange(0, 1, 101)), label=false, color="black", linestyle=:dash, linewidth=1.5)
    plot!(distribution, LinRange(0, 1, 101), poisson.(LinRange(0, 1, 101)), label=false, color="black", linewidth=1.5)
    annotate!(distribution, 0.15, 0.2, text("\$P_{\\mathrm{GOE}}(r)\$", 18, :black))
    annotate!(distribution, 0.15, 1.9, text("\$P_{\\mathrm{Poi}}(r)\$", 18, :black))
    savefig(distribution, "./figures/GapRatio_Distribution_L=$L.svg")
end

##############Distribution for finite size scaling #############

L_values = [8, 10, 12, 14]
W = Float64(8)
bins = [65, 60, 50, 30]
colors = ["orange", "green", "blue", "red"]

finite_size = histogram(xlabel="\$r_n\$", ylabel="\$P(r_n)\$", xlabelfontsize=14, ylabelfontsize=14, legendfontsize=12, dpi=200, ylim=(0, Inf), xlim=(0, 1))
for (L, color, nbins) in zip(L_values, colors, bins)
    file = jldopen("./data/GapRatioDistribution_L=$L.jld", "r")
    Wv = read(file, "W = $W")
    stephist!(Wv, label="\$L = $L\$", alpha=0.8, normed=true, linewidth=1.5, bins=LinRange(0, 1, nbins + 1), color=color)
    close(file)
end
plot!(finite_size, LinRange(0, 1, 101), GOE.(LinRange(0, 1, 101)), label="\$P_{\\mathrm{GOE}}(r)\$", color="black", linestyle=:dash, linewidth=1.5)
plot!(finite_size, LinRange(0, 1, 101), poisson.(LinRange(0, 1, 101)), label="\$P_{\\mathrm{Poi}}(r)\$", color="black", linewidth=1.5)
savefig(finite_size, "./figures/GapRatio_FiniteSizeScaling.svg")


################ Calculation disorder average of gap ratio #################

L_values = [8, 10, 12, 14]
colors = [:blue, :green, :red, :magenta]

file = jldopen("./data/GapRatioAverage.jld", "r")
average = plot(ylabel="\$[r_n]\$", xlabel="\$W/J\$", xscale=:log10, xlabelfontsize=14, ylabelfontsize=14, legendfontsize=12, dpi=200, legend=:bottomleft, minorgrid=true)
W_values = read(file, "W")
for (L, color) in zip(L_values, colors)
    rn_average = read(file, "L = $L")
    plot!(W_values, rn_average, label="\$L = $L\$", alpha=0.7, linewdith=1.5, color=color)
    scatter!(W_values, rn_average, label=false, color=color)
end
close(file)
x_fill = [6, 6, 10, 10]
y_fill = [0, 1, 1, 0]
plot!(x_fill, y_fill, seriestype=:shape, color=:yellow, alpha=0.2, label="")
xlims!(1, 29)
ylims!(0.37, 0.54)
hline!([0.53], color=:black, alpha=0.8, label=false, linestyle=:dash)
annotate!(average, 21, 0.525, text("GOE", 12, :black))
hline!([2 * log(2) - 1], color=:black, alpha=0.8, label=false, linestyle=:dash)
annotate!(average, 21, 0.3775, text("Poisson", 12, :black))
savefig(average, "./figures/GapRatio_DisorderAverage.svg")

##################### Measurement of <S^z_j>

L_values = [8, 10, 12, 14]
W_values = Float64.([0.5, 3, 6, 8, 12])
bins = bins = [300, 250, 200, 150]
colors = [:purple, :orange, :green, :red, :blue]

for (L, nbins) in zip(L_values, bins)
    distribution = histogram(xlabel="\$<S_l^z>\$", ylabel="\$P(<S_l^z>)\$", bins=nbins, xlabelfontsize=20, ylabelfontsize=20, xtickfontsize=12, ytickfontsize=12, legendfontsize=18, dpi=200, minorgrid=true)
    file = jldopen("./data/Sz_Expectation_L=$L.jld", "r")
    for (W, color) in zip(W_values, colors)
        Sz_values = read(file, "W = $W")
        stephist!(Sz_values, label="\$W/J = $W\$", alpha=0.8, normed=true, linewidth=1.5, bins=LinRange(-0.5, 0.5, nbins + 1), color=color)
        ylims!(0, Inf)
        xlims!(-0.5, 0.5)
    end
    savefig(distribution, "./figures/Sz_Distribution_L$(L).svg")
    close(file)
end

##################### Statistics ####################

L_values = [8, 10]
W_values = Float64.([2, 15])
colors = [:red, :blue]

for L in L_values
    file = jldopen("./data/Statistics_L=$L.jld", "r")
    Nr_values = read(file, "Nr_values")
    devs = plot(ylabel="\$\\Delta[r_n]/\\overline{[r_n]}\$", xlabel="\$N_r\$", xscale=:log10, xlabelfontsize=24, ylabelfontsize=24, xtickfontsize=12, ytickfontsize=12, legendfontsize=16, dpi=200, minorgrid=true)
    vars = plot(ylabel="\$\\sigma^2\$", xlabel="\$N_r\$", xscale=:log10, xlabelfontsize=24, ylabelfontsize=24, xtickfontsize=12, ytickfontsize=12, legendfontsize=16, dpi=200, minorgrid=true)
    for i in 1:5
        labell = false

        for (W, color) in zip(W_values, colors)
            if i == 1
                labell = "\$W/J =$W\$"
            end

            scatter!(devs, Nr_values[2:end], read(file, "$(i)RelDeviations_$W"), color=color, label=labell, markersize=2.5, alpha=0.8)
            plot!(devs, Nr_values[2:end], read(file, "$(i)RelDeviations_$W"), color=color, label=false, alpha=0.8)
            scatter!(vars, Nr_values, read(file, "$(i)Variances_$W"), color=color, label=labell, markersize=2.5, alpha=0.8)
            plot!(vars, Nr_values, read(file, "$(i)Variances_$W"), color=color, label=false, alpha=0.8)

        end
    end
    savefig(devs, "./figures/Deviation_StatisticsL=$L.svg")
    savefig(vars, "./figures/Variance_StatisticsL=$L.svg")
    close(file)
end
