using Plots
using LinearAlgebra
using KernelDensity
using LaTeXStrings


############### ONLY MAGNETIZATION SYMMETRY #####################
let
    function flip(s::Integer, i::Int, j::Int)

        # XOR operation to flip the bits
        s = s ⊻ (1 << (i - 1))
        s = s ⊻ (1 << (j - 1))

        return s
    end

    function generate_hamiltonian(N::Int)

        H = zeros(2^N, 2^N)

        # Attention on indexing in Julia, starting at 1 going to 2^N allows to index in Hamiltonian
        # with H[s,s], however ground state all spins down is now at state s = 2^N instead s = 0
        for s in 1:2^N
            for i in 1:N
                j = mod1(i + 1, N)
                if ((s >> (i - 1)) & 1) == ((s >> (j - 1)) & 1)
                    H[s, s] += 1 / 4 # Number of state a = s in this case
                else
                    H[s, s] -= 1 / 4
                    b = flip(s, i, j)
                    H[b, s] = 1 / 2
                end
            end
        end
        return H
    end

    function magnetization_basis(n_up::Int, N::Int) # Returns a Basis of a given Magnetization n_up for N Spins
        basis = []
        for s in 1:2^N
            bit_sum = 0
            for i in 1:N
                bit_sum += (s >> (i - 1)) & 1
            end
            if bit_sum == n_up
                push!(basis, s)
            end
        end
        return basis
    end

    function find_state(s::Integer, basis::Array) # Returns State Position of State s in Basis basis
        M = length(basis)
        b_min, b_max = 1, M

        while b_min <= b_max
            b = b_min + (b_max - b_min) ÷ 2
            if s < basis[b]
                b_max = b - 1
            elseif s > basis[b]
                b_min = b + 1
            else
                return b
            end
        end
        return -1
    end

    function magnetization_block_hamiltonian(n_up::Int, N::Int) # Returns Block_Hamiltonian and respective Basis for given Magnetization n_up of N Spins

        sub_basis = magnetization_basis(n_up, N)
        M = length(sub_basis)
        sub_H = zeros(M, M)

        for a in 1:M
            for i in 1:N
                j = mod1(i + 1, N)
                if ((sub_basis[a] >> (i - 1)) & 1) == ((sub_basis[a] >> (j - 1)) & 1)
                    sub_H[a, a] += 1 / 4
                else
                    sub_H[a, a] -= 1 / 4
                    s = flip(sub_basis[a], i, j)
                    b = find_state(s, sub_basis)
                    sub_H[b, a] = 1 / 2
                end
            end
        end
        return sub_H, sub_basis
    end

    function magnetization_spectrum(N::Int) # Return Hamiltonian in Magentization Eigenbasis and Basis

        spectrum = []

        for n_up in 0:N

            append!(spectrum, eigvals(magnetization_block_hamiltonian(n_up, N)[1]))

        end


        return sort(spectrum)
    end

    ###### Plot the Spectrum #################
    N = 18
    spectrum = magnetization_spectrum(N)
    plt = plot(1:length(spectrum), real.(spectrum), label=false, xlabel="\$n\$", ylabel="\$E_n / J\$", color="red", legendfontsize=12, xlabelfontsize=14, ylabelfontsize=14, dpi=200)
    annotate!(0.85 * length(spectrum), 0.9 * real.(spectrum)[1], text("\$E_0 / LJ = $(round((spectrum[1]/N), digits = 3))\$", 12))
    savefig(plt, "./figures/Heisenberg_Spectrum.svg")

end

#################### SEMI-MOMENTUM SYMMETRY ###################

# Flips spins/bits i and j of state s
function flip(s::Integer, i::Int, j::Int)

    # XOR operation to flip the bits
    s = s ⊻ (1 << (i - 1))
    s = s ⊻ (1 << (j - 1))

    return s
end

# Returns position of state s in basis, returns -1 of not found
function find_state(s::Integer, basis::Array) # Returns State Position of State s in Basis basis
    M = length(basis)
    b_min, b_max = 1, M

    while b_min <= b_max
        b = b_min + (b_max - b_min) ÷ 2
        if s < basis[b]
            b_max = b - 1
        elseif s > basis[b]
            b_min = b + 1
        else
            return b
        end
    end
    return -1
end

# Returning magnetization_basis for n_up Up-Spins in all 2^N Spinconfigurations / states
function magnetization_basis(n_up::Int, N::Int) # Returns a Basis of a given Magnetization n_up for N Spins
    basis = []
    for s in 1:2^N
        bit_sum = 0
        for i in 1:N
            bit_sum += (s >> (i - 1)) & 1
        end
        if bit_sum == n_up
            push!(basis, s)
        end
    end
    return basis
end

# Cyclic shift of the first n bits of Integer t
function cyclebits(t::Integer, n::Integer)

    if t == 2^n
        return t
    end

    mask = (1 << n) - 1
    msb = (t >> (n - 1)) & 1  # Most Significant Bit (MSB) sichern
    shifted = ((t << 1) & mask) | msb  # Verschiebe die Bits um eine Stelle nach links, MSB wiederherstellen
    return shifted
end

# Reflecting first n bits of Integer t
function reflectbits(t::Integer, n::Int)

    if t == 2^n
        return t
    end

    return (bitreverse(Int64(t)) >>> (64 - n))
end

# Checking if the given state is a valid representative of a semimomentum_state with parity
function check_state(s::Int, k::Float64, N::Int)    # Returns periodicity R of the given state
    # Checking if state s is a valid new representative of a valid momentum state (periodic k = 2pi/R_a)
    t, R = s, -1
    for i in 1:N
        t = cyclebits(t, N)
        if t < s
            break
        elseif t == s
            if (mod(k * i, N) != 0)
                break
            end
            R = i
            break
        end
    end
    t, m = reflectbits(s, N), -1
    for i in 0:R-1
        if t < s
            R = -1
            break
        elseif t == s
            m = i
            break
        end
        t = cyclebits(t, N)
    end
    return R, m
end

# Returns the representative of a given semimomentum_state an the parameters l and q for H-Element
function representative(s::Int, N::Int)
    r, t, l = s, s, 0
    for i in 1:N
        t = cyclebits(t, N)
        if t < r
            r = t
            l = i
        end
    end

    t, q = reflectbits(s, N), 0
    for i in 1:(N)
        t = cyclebits(t, N)
        if t < r
            r = t
            l = i
            q = 1
        end
    end
    return r, l, q
end

# Returns semimomentum_basis bith parity for state (n_up, k, p), to do so it tests all states for a given magnetization_basis to n_up Up-Spins of 2^N Configurations
function semimomentum_basis(k::Float64, p::Int, N::Int, magnetization_basis::Array)
    basis, periodicity, translations = [], [], []

    for s in magnetization_basis

        R_a, m = check_state(s, k, N)

        if k == 0 || k == N / 2 + 0.5 * (N % 2)
            sigm_lis = [1]
        else
            sigm_lis = [1, -1]
        end

        for sigm in sigm_lis
            R = R_a
            if m != -1
                if (1 + sigm * p * cos(2 * pi * k * m / N)) == 0
                    R = -1
                end
                if (sigm == -1) && ((1 - sigm * p * cos(2 * pi * k * m / N)) != 0)
                    R = -1
                end
            end

            if R >= 0
                push!(basis, s)
                push!(periodicity, sigm * R)
                push!(translations, m)
            end
        end

    end
    return basis, periodicity, translations
end

# Builds off-diagonal Matrix element H[b, a] for the semimomentum_state s[a] of (k, p) with H |s[a]> = |s[b]> where s[b] has parameters l, q
function H_element(a::Int, b::Int, l::Int, q::Int, k::Float64, p::Int, basis::Array, periodicity::Array, translations::Array, N::Int)

    k *= (2 * pi / N)

    N_a = (translations[a] != -1) ? (1 + (periodicity[a] / abs(periodicity[a]) * p * cos(k * translations[a]))) / abs(periodicity[a]) : 1 / abs(periodicity[a])
    N_b = (translations[b] != -1) ? (1 + (periodicity[b] / abs(periodicity[b]) * p * cos(k * translations[b]))) / abs(periodicity[b]) : 1 / abs(periodicity[b])

    H = 1 / 2 * (periodicity[a] / abs(periodicity[a]) * p)^q * sqrt(N_b / N_a)

    m_b = -1
    r, t = reflectbits(basis[b], N), basis[b]
    for i in 1:N
        t = cyclebits(t, N)
        if t == r
            m_b = i % N
        end
    end

    if periodicity[a] * periodicity[b] > 0
        if m_b == -1
            H *= cos(k * l)
        else
            H *= (cos(k * l) + periodicity[a] / abs(periodicity[a]) * p * cos(k * (l - m_b))) / (1 + periodicity[a] / abs(periodicity[a]) * p * cos(k * m_b))
        end
    else
        if m_b == -1
            H *= -periodicity[a] / abs(periodicity[a]) * sin(k * l)
        else
            H *= (-periodicity[a] / abs(periodicity[a]) * sin(k * l) + p * sin(k * (l - m_b))) / (1 - periodicity[a] / abs(periodicity[a]) * p * cos(k * m_b))
        end
    end

    return H
end

# Returns Sub_Block_Hamiltonian for given semimomentum_basis to (k, p) in the n_up Magnetization Block
function magnetization_semimomentum_block_hamiltonian(n_up::Int, k::Float64, p::Int, N::Int)

    magnetization_b = magnetization_basis(n_up, N)

    sub_basis, sub_periodicity, sub_translations = semimomentum_basis(k, p, N, magnetization_b)
    M = length(sub_basis)
    sub_H = zeros(M, M)

    for a in 1:M

        if (a > 1) && (sub_basis[a] == sub_basis[a-1])
            continue
        elseif (a < M) && (sub_basis[a] == sub_basis[a+1])
            n = 2
        else
            n = 1
        end

        E_z = 0
        for i in 1:N
            j = mod1(i + 1, N)
            if ((sub_basis[a] >> (i - 1)) & 1) == ((sub_basis[a] >> (j - 1)) & 1)
                E_z += 1 / 4
            else
                E_z -= 1 / 4
                # Here off Diagonal Terms
                s = flip(sub_basis[a], i, j)
                r, l, q = representative(s, N)
                b = find_state(r, sub_basis)
                if b > 0
                    if (b > 1) && (sub_basis[b] == sub_basis[b-1])
                        m = 2
                        b -= 1
                    elseif (b < M) && (sub_basis[b] == sub_basis[b+1])
                        m = 2
                    else
                        m = 1
                    end

                    for x in b:(b+m-1)
                        for y in a:(a+n-1)
                            sub_H[x, y] += H_element(y, x, l, q, k, p, sub_basis, sub_periodicity, sub_translations, N)
                        end
                    end
                end
            end
        end

        # Add Diagonal Terms
        for i in a:(a+n-1)
            sub_H[i, i] += E_z
        end

    end
    return sub_H
end

# Returns Spectrum of Hamiltonian using blocks parametrized by (m_z,k,p) for m_z = -N/2,..,N/2, k = 2pi/N * m, m = 0, ... , N/2 and p = +/- 1
function hamiltonian_spectrum(N::Int)

    spectrum = []
    for n_up in 0:N
        for k in 0:(N÷2)
            for p in [1, -1]
                sub_H = magnetization_semimomentum_block_hamiltonian(n_up, Float64(k), p, N)
                append!(spectrum, eigvals(sub_H))
            end
        end
    end

    return sort(real.(spectrum))
end

# Returns GS-Energy of Hamiltonian using blocks parametrized by (m_z,k,p) for m_z = -N/2,..,N/2, k = 2pi/N * m, m = 0, ... , N/2 and p = +/- 1
function hamiltonian_gs(N::Int)
    spectrum = []
    n_up = Int(N / 2)
    if N % 4 == 0
        k = 0
        p = 1
    elseif N % 4 == 2
        k = Int(N / 2)
        p = -1
    else
        exit()
    end
    sub_H = magnetization_semimomentum_block_hamiltonian(n_up, Float64(k), p, N)
    append!(spectrum, eigvals(sub_H))

    return sort(real.(spectrum))[1]
end

############# Plot E_0/L over 1/L ################
## GS - Energy should converge to E_0 = 1/4 - ln(2) ≘ -0.44315 ##

L_values = [4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24]

gs_energy = []
for L in L_values
    gs = hamiltonian_gs(L)
    append!(gs_energy, gs)
end
println(gs_energy)
plt = hline([1 / 4 - log(2)], label=L"$E_{\infty}/LJ = -0.443$", color="green", linestyle=:dash, xlim=(0, 0.255), xlabelfontsize=14, ylabelfontsize=14, legendfontsize=12, dpi=200)
scatter!(1 ./ L_values, gs_energy ./ L_values, xlabel="\$1 / L\$", ylabel="\$E_0 / LJ\$", ylim=(-0.503, -0.44), label=false, color="red", linewidth=1.5)
plot!(1 ./ L_values, gs_energy ./ L_values, color="red", label=false)
savefig(plt, "./figures/GS_over_L_withSM.svg")

###### Print Out Subblock Dimensions for different Quantum Numbers #########
N_values = [12, 16, 20, 24, 28, 32]

file = open("./data/Semimomentum_SubblockDims.csv", "w")
println(file, "N, k, p, dim")

for N in N_values
    n_up = Int(N / 2)
    m_basis = magnetization_basis(n_up, N)
    for k in 0:1
        for p in [1, -1]
            basis, _, _ = semimomentum_basis(Float64(k), p, N, m_basis)
            println(file, "$N, $k, $p, $(length(basis))")
        end
    end
end
close(file)
